#include "AddCarEvent.h"
#include "TrafficSimulation.h"
#include "Common.h"
#include "Event.h"

AddCarEvent::AddCarEvent(int timeOfExecution, string &carID, string &roadPlan, TrafficSimulation *sim) :
Event(timeOfExecution),  _carId(carID), _roadPlan(roadPlan), _sim(sim){

}

void AddCarEvent::performEvent() {
	_sim->addCar(_carId, _roadPlan);
}

AddCarEvent::~AddCarEvent() {
}

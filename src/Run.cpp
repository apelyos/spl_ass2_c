#include "Common.h"
#include "TrafficSimulation.h"

string CONFIGUARTION_INI = "Configuration.ini";
string ROADMAP_INI = "RoadMap.ini";
string COMMANS_INI = "Commands.ini";
string EVENTS_INI = "Events.ini";
string REPORTS_INI = "Reports.ini";

int main() {
	cout << "Simulation main start.\n\n";

	cout << "Creating Simulation Objects...\n";

	// Initiate a new simulation object
	TrafficSimulation *sim = new TrafficSimulation(CONFIGUARTION_INI, ROADMAP_INI, COMMANS_INI, EVENTS_INI, REPORTS_INI);
	
	cout << "Running Simulation...\n\n";

	// Safely try to run the simulation
	try {
		sim->runSimulation();
	}
	catch (const exception& e){
		cout << "Exception occurred during execution: " << e.what() << endl;
	}

	// Destroy the object
	delete sim;

	cout << "Simulation main end.\n";

	return 0;
}



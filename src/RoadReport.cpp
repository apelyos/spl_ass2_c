#include "RoadReport.h"
#include "Report.h"
#include "Common.h"
#include "Car.h"
#include "Junction.h"

RoadReport::RoadReport(int timeOfExecution, string &id, Road* road, boost::property_tree::ptree& reportsPt, TrafficSimulation& sim) :
Report(id, timeOfExecution, reportsPt, sim) ,_road (road) {

}

void RoadReport::writeReport(){
	_reportsPt.add(_id + ".startJunction", _road->getStartJunction()->getId());
	_reportsPt.add(_id + ".endJunction", _road->getEndJunction()->getId());

	// Get cars in current road
	vector<Car*> cars = _road->getCars();

	// Concatenate and build string
	string carsToWrite = "";
	for (int i = int(cars.size() - 1); i >= 0; --i){
		carsToWrite = carsToWrite + "(" + cars.at(i)->getId() + "," + boost::lexical_cast<string>(cars.at(i)->getDistanceFromStart()) + ")";
	}
	_reportsPt.add(_id + ".cars", carsToWrite);
}

RoadReport::~RoadReport()
{
}

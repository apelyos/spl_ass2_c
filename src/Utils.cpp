#include "Common.h"
#include "Utils.h"

const bool ENABLE_LOGGER = false;

// Convenient logger function
void logger(string msg) {
	if (ENABLE_LOGGER)	cout << "DEBUG: " << msg << endl;
}

// convert int to string
string to_string (int num) {
	ostringstream convert;   // stream used for the conversion
	convert << num;      // insert the textual representation of 'Number' in the characters in the stream
	return convert.str();
}

// convert string to int
int stoi (string str) {
	return atoi( str.c_str() );
}

// Split a sentence into words by a specified delimiter character
vector<string> split(string sentence, char delimiter){
	string curr = "";

	vector<string> res;

	for (unsigned i = 0; i < sentence.length(); i++) {
		if (sentence[i] != delimiter) {
			curr += sentence[i];
		}
		else {
			if (!curr.empty()){
				res.insert(res.end(), curr);
			}
			curr = "";
		}
	}

	if (!curr.empty()){
		res.insert(res.end(), curr);
	}

	return res;
}

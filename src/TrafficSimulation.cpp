#include "TrafficSimulation.h"
#include "Common.h"

int TrafficSimulation::MAX_SPEED;
int TrafficSimulation::DEFAULT_TIME_SLICE;
int TrafficSimulation::MAX_TIME_SLICE;
int TrafficSimulation::MIN_TIME_SLICE;
int TrafficSimulation::_currentTime;

TrafficSimulation::TrafficSimulation(string &configurationIni, string &roadMapIni, string &commandsIni, string &eventsIni, string &reportsIni) :
		_configurationIni (configurationIni), _roadMapIni(roadMapIni) , _commandsIni(commandsIni),
		_eventsIni(eventsIni), _reportsIni(reportsIni), _terminationTime(-1), _cars() , _junctions(),
		_roads(), _reports(), _events(), _reportsPt()
{

	// Initiate Property tree for the reports
	_reportsPt = new boost::property_tree::ptree();

	// Set the beginning time to 1
	_currentTime = 1;
}

void TrafficSimulation::runSimulation(){
	// Initiate simulation parameters and objects
	initiate();

	logger("Starting Simulation");

	while (!isEndOfSimulation()){
		logger("******** New simulation iteration - time=" + to_string(_currentTime) + " ********");
		
		// Perform Events 
		try{
			while (!_events.empty() && _currentTime == _events.begin()->first){
				logger("Performing event");
				_events.begin()->second->performEvent();
				delete _events.begin()->second;
				_events.erase(_events.begin());
			}
		}
		catch (const exception& e){
			cout<< "Couldn't perform events. Exception: "<<e.what()<<endl;
			throw e;
		}

		// Update cars history
		try{
			updateCarHistory();
		}
		catch (const exception& e){
			cout << "Couldn't update cars history. Exception: " << e.what() << endl;
			throw e;
		}
		
		// Perform (Commands) Reports 
		try{
			while (!_reports.empty() &&  _currentTime == _reports.begin()->first){
				logger("Writing a report");
				Report* rep = _reports.begin()->second;
				rep->writeReport();
				delete _reports.begin()->second;
				_reports.erase(_reports.begin());
			}
		}
		catch (const exception& e){
			cout<< "Couldn't write reports. Exception: "<<e.what()<<endl;
			throw e;
		}

		// Advance Cars In Road 
		try{
			for (std::map<string, Road*>::iterator roadsIt = _roads.begin(); roadsIt != _roads.end(); ++roadsIt){
				logger("Advancing in road:" + roadsIt->first);
				roadsIt->second->advanceCars();
			}
		}
		catch (const exception& e){
			cout << "Couldn't perform advancing in roads. Exception: " << e.what() << endl;
			throw e;
		}

		// Advance Cars In Junctions 
		try{
			for (std::map<string, Junction*>::iterator junctionIt = _junctions.begin(); junctionIt != _junctions.end(); ++junctionIt){
				logger("Advancing in junction:" + junctionIt->first);
				junctionIt->second->advanceCars();
			}
		}
		catch (const exception& e){
			cout << "Couldn't perform advancing in junctions. Exception: " << e.what() << endl;
			throw e;
		}

		// Advance the simulation time
		updateTime();
	}

	// Write the output report
	logger("Writing reports to 'Reports.ini'");

	try{
		write_ini(_reportsIni, *_reportsPt);
	}
	catch (const exception& e){
		cout << "Problem writing reports to 'Reports.ini'. Exception: " << e.what() << endl;
		throw e;
	}

	logger("Simulation Finished with great success.");
}

void TrafficSimulation::updateTime(){
	_currentTime++;
}

int TrafficSimulation::getCurrentTime(){
	return _currentTime;
}

bool TrafficSimulation::isEndOfSimulation(){
	return _currentTime == _terminationTime;
}

void TrafficSimulation::initiate(){
	try{
		initiateConfiguration();
	}
	catch (exception& e){
		cout << "Failed to load from Configuraion.ini with exception: " << e.what() << endl;
		throw e;
	}

	try
	{
		initiateRoadMap();
	}
	catch (exception& e){
		cout << "Failed to load from RoadMap.ini with exception: " << e.what() << endl;
		throw e;
	}

	try
	{
		initiateCommands();
	}
	catch (exception& e){
		cout << "Failed to load from Commands.ini with exception: " << e.what() << endl;
		throw e;
	}

	try
	{
		initiateEvents();
	}
	catch (exception& e){
		cout << "Failed to load from Events.ini with exception: " << e.what() << endl;
		throw e;
	}
}

void TrafficSimulation::initiateConfiguration(){
	boost::property_tree::ptree pt;
	boost::property_tree::ini_parser::read_ini(_configurationIni, pt);
	MAX_SPEED = pt.get<int>("Configuration.MAX_SPEED");
	DEFAULT_TIME_SLICE = pt.get<int>("Configuration.DEFAULT_TIME_SLICE");
	MAX_TIME_SLICE = pt.get<int>("Configuration.MAX_TIME_SLICE");
	MIN_TIME_SLICE = pt.get<int>("Configuration.MIN_TIME_SLICE");
}

void TrafficSimulation::initiateRoadMap(){
	boost::property_tree::ptree pt;
	boost::property_tree::ini_parser::read_ini(_roadMapIni, pt);

	//first - insert all junction
	for (boost::property_tree::ptree::const_iterator section = pt.begin(); section != pt.end(); section++) {
		addJunction(section->first.data());
	}

	//second - insert roads by each end-junction
	for (boost::property_tree::ptree::const_iterator section = pt.begin(); section != pt.end(); section++) {
		string endJunction = section->first.data();
		for (boost::property_tree::ptree::const_iterator property = section->second.begin(); property != section->second.end(); property++) {
			string startJunction = property->first.data();
			int length = boost::lexical_cast<int>(property->second.data());
			addRoad(startJunction + "->" + endJunction, startJunction, endJunction, length);
		}
	}
}

void TrafficSimulation::initiateEvents(){
	boost::property_tree::ptree pt;
	boost::property_tree::ini_parser::read_ini(_eventsIni, pt);

	for (boost::property_tree::ptree::const_iterator section = pt.begin(); section != pt.end(); section++) {
		try{
			string eventId = section->first.data();
			string type = section->second.get<string>("type");
			int time = section->second.get<int>("time");
			string carId = section->second.get<string>("carId");
			if (type == "car_fault"){
				int timeOfFault = section->second.get<int>("timeOfFault");
				Event* event = new CarFaultEvent(time, carId, timeOfFault, this);
				_events.insert(std::pair<int, Event*>(time, event));

				logger("Car fault event added- car id:" + carId + " time:" + to_string(time) + " timeOfFault:" + to_string(timeOfFault));
			}
			else if (type == "car_arrival"){
				string roadPlan = section->second.get<string>("roadPlan");
				Event* event = new AddCarEvent(time, carId, roadPlan, this);
				_events.insert(std::pair<int, Event*>(time, event));
				logger("Car arrival event added- car id:" + carId + " time:" + to_string(time) + " road-plan:" + roadPlan);
			}
			else {
				logger("Unknown event type:'" + type + "'. Moving to the next one");
			}
		}
		catch (const std::exception& e){
			cout<<"Problem fetching an event, moving to the next one. Exception:" << e.what()<<endl;
		}
		
	}
	
}

void TrafficSimulation::initiateCommands(){
	boost::property_tree::ptree pt;
	boost::property_tree::ini_parser::read_ini(_commandsIni, pt);

	for (boost::property_tree::ptree::const_iterator section = pt.begin(); section != pt.end(); section++) {
		try{
			//string commandId = section->first.data();
			std::map<string, string> fields;
			for (boost::property_tree::ptree::const_iterator property = section->second.begin(); property != section->second.end(); property++) {
				fields.insert(std::pair<string, string>(property->first.data(), property->second.data()));
			}
			enqueueCommand(fields);
		}
		catch (const std::exception& e){
			cout << "Problem fetching a command, moving to the next one. Exception:" << e.what() << endl;
		}

	}

}

void TrafficSimulation::enqueueCommand(std::map<string,string> fields){
	Report* report;
	try{
		if (fields["type"] == "termination"){
			_terminationTime = stoi(fields["time"]);
			return;
		}
		else if (fields["type"] == "car_report"){
			report = new CarReport(stoi(fields["time"]), fields["id"], fields["carId"], *_reportsPt, *this);
		}
		else if (fields["type"] == "road_report"){
			report = new RoadReport(stoi(fields["time"]), fields["id"], getRoad(fields["startJunction"]+"->"+fields["endJunction"]), *_reportsPt, *this);
		}
		else if (fields["type"] == "junction_report"){
			report = new JunctionReport(stoi(fields["time"]), fields["id"], getJunction(fields["junctionId"]), *_reportsPt, *this);
		}
		else{
			logger("Unknown command type:" + fields["type"]);
		}
	}
	catch (const std::exception& e){
		cout << "Problem fetching a command, skipping it. Exception:" << e.what() << endl;
	}
	_reports.insert(std::pair<int, Report*>(stoi(fields["time"]), report));
	
}

void TrafficSimulation::addJunction(string id){
	Junction* junction = new Junction(id);
	_junctions.insert(std::pair<string,Junction*> (id, junction));
	logger("Junction '" + id + "' was inserted");

}

void TrafficSimulation::addRoad(string id, string startJunction, string endJunction, int length){
	Road* road = new Road(_junctions[startJunction], _junctions[endJunction], length);
	_roads.insert(std::pair<string, Road*>(id, road));
	logger("Road '" + id + "' was inserted");
}

void TrafficSimulation::addCar(string carId, string roadPlan){
	Car* car = new Car(carId);
	vector<string> junctions = split(roadPlan, ',');
	for (size_t i = 0; i < junctions.size(); i++) {
		if (i != 0){
			try {
				Road* road = getRoad(junctions[i - 1] + "->" + junctions[i]);
				car->enqueuePlanItem(road);
			}
			catch (exception& e){
				logger("Failed adding road " + junctions[i - 1] + "->" + junctions[i] + " to car id=" + carId+". Moving to the next one. Exception:"+e.what());
			}
		}
	}
	_cars.insert(std::pair<string, Car*>(carId, car));
	logger("Car '" + carId + "' was inserted");
}

Car* TrafficSimulation::getCar(string id){
	try{
		Car* car = _cars.at(id);
		return car;
	}
	catch (exception &e){
		logger("Couldn't find car id=" + id);
		return NULL;
	}
}

Road* TrafficSimulation::getRoad(string id){
	try{
		Road* road = _roads.at(id);
		return road;
	}
	catch (exception &e){
		logger("Couldn't find road id=" + id);
		return NULL;
	}
}

Junction* TrafficSimulation::getJunction(string id){
	try{
		Junction* junction = _junctions.at(id);
		return junction;
	}
	catch (exception &e){
		logger("Couldn't find junction id=" + id);
		return NULL;
	}
}



void TrafficSimulation::updateCarHistory(){
	for (map<string, Car *>::iterator carIt = _cars.begin(); carIt != _cars.end(); ++carIt){
		carIt->second->addHistoryEvent();
	}
}

TrafficSimulation::~TrafficSimulation()
{
	logger("****** Destructing Objects ******");
	for (map<string, Car *>::iterator carIt = _cars.begin(); carIt != _cars.end(); ++carIt){
		delete carIt->second;
	}
	
	for (map<string, Junction *>::iterator junctionIt = _junctions.begin(); junctionIt != _junctions.end(); ++junctionIt){
		delete junctionIt->second;
	}

	for (map<string, Road *>::iterator roadIt = _roads.begin(); roadIt != _roads.end(); ++roadIt){
		delete roadIt->second;
	}
	
	for (multimap<int, Report*>::iterator reportIt = _reports.begin(); reportIt != _reports.end(); ++reportIt){
		delete reportIt->second;
	}

	for (multimap<int, Event*>::iterator eventIt = _events.begin(); eventIt != _events.end(); ++eventIt){
		delete eventIt->second;
	}

	delete _reportsPt;
}

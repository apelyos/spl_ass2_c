#include "Road.h"
#include "Junction.h"
#include "Car.h"
#include "Common.h"
#include "TrafficSimulation.h"

const double SPEED_DECREASE_FACTOR = 0.5; // for faulty cars
const string ROAD_ID_SEPERATOR = "->";

Road::Road(Junction *startJunction, Junction *endJunction, int length) :
_id() ,	_startJunction(startJunction), _endJunction(endJunction), _cars(), _length (length) {
	logger("Road CTOR with Junctions: " + startJunction->getId() + ROAD_ID_SEPERATOR + endJunction->getId());

	// We build our own ID for the car
	_id = startJunction->getId() + ROAD_ID_SEPERATOR + endJunction->getId();

	// add this road to the incoming roads in the endJunction
	_endJunction->addIncomingRoad(this);
}

string Road::getID(){
	return _id;
}

double Road::getDrivingSpeed(int numOfFaultyCars) {
	// Calculate the base road speed according to the specification
	double baseSpeed = ceil((double)_length / (double)_cars.size());

	// Calculate the speed with all the faulty cars ahead
	double speed = ceil(baseSpeed * (pow(SPEED_DECREASE_FACTOR, numOfFaultyCars)));
	
	// Checking if we've went over the speed limit
	if (speed > TrafficSimulation::MAX_SPEED) 
		return TrafficSimulation::MAX_SPEED;
	else 
		return speed;
}

// Return + Delete the first not faulty car that awaits at the end of the road
Car* Road::dequeueOutgoingCar() {
	logger("Road::dequeueOutgoingCar in road: " + _id);

	if (_cars.empty()) {
		return NULL;
	}

	vector<Car*>::iterator it = _cars.begin();

	while (it != _cars.end() && (*it)->getDistanceFromStart() == _length) {
		if ((*it)->isFaulty()) {
			logger("Road::dequeueOutgoingCar car: " + (*it)->getId() + " is faulty in junction.");
			it++;
		}
		else {
			Car *car = (*it);
			_cars.erase(it);
			return (car);
		}
	}

	return NULL;
}

void Road::advanceCars() { 
	logger("Road::advanceCars, Road: " + _id);

	if (_cars.size() == 0) return; // If I dont have any cars, exit

	int faultyCounter = 0;

	// stable-sorting the array (reverse - biggest distance at start)
	stable_sort(_cars.begin(),_cars.end(),compareByDistanceFromStart);

	// define iterator
	vector<Car*>::iterator it = _cars.begin();

	// Iterate cars
	while (it != _cars.end()) {

		// Update car history
		//(*it)->addHistoryEvent();

		if ((*it)->isFaulty()) faultyCounter++; //If faulty Increase faulty counter

		// Advance car at the appropriate speed
		(*it)->advanceCarInCurrentRoad(getDrivingSpeed(faultyCounter));
		
		it++; //get the next iterator
	}
}

Junction* Road::getStartJunction() {
	return _startJunction;
}

Junction* Road::getEndJunction() {
	return _endJunction;
}

// Add a car to this road
void Road::addCarToRoad(Car *car) {
	logger("Road::addCarToRoad, carID: " + car->getId() + " roadID: " + _id); // add road ID
	_cars.insert(_cars.end(), car);
}

vector<Car*> Road::getCars() {
	return _cars;
}

// Return all the cars standing in line at the end of the road(waiting to pass the junction)
vector<Car*> Road::getCarsInOutgoingQueue() {
	vector<Car*> res;

	vector<Car*>::iterator it = _cars.begin();

	while (it != _cars.end() && (*it)->getDistanceFromStart() == _length) {
		res.insert(res.end(), (*it));
		it++;
	}

	return res;
}

// Predicate to compare by the distance of car objects (pointers)
bool compareByDistanceFromStart(Car* a, Car* b) {
	return a->getDistanceFromStart() > b->getDistanceFromStart(); // Reverse order, from biggest to lowest
}

int Road::getLength(){
	return _length;
}

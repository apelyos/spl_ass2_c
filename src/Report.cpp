#include "Common.h"
#include "Report.h"

Report::Report(string id, int timeOfExecution, boost::property_tree::ptree& reportsPt, TrafficSimulation& sim) : _id(id), _timeOfExecution(timeOfExecution), _reportsPt(reportsPt), _sim(sim){

}

Report::~Report() {

}

int Report::getTimeOfExecution() {
	return _timeOfExecution;
}

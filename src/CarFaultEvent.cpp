#include "CarFaultEvent.h"
#include "TrafficSimulation.h"
#include "Common.h"
#include "Car.h"
#include "Event.h"


CarFaultEvent::CarFaultEvent(int timeOfExecution, string &carID, int faultDuration, TrafficSimulation *sim) :
Event(timeOfExecution), _carId(carID),  _faultDuration(faultDuration), _sim(sim){

}

void CarFaultEvent::performEvent() {
	_sim->getCar(_carId)->markAsFaulty(_faultDuration);
}

CarFaultEvent::~CarFaultEvent() {
}

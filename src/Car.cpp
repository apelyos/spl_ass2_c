#include "Car.h"
#include "Road.h"
#include "Junction.h"
#include "TrafficSimulation.h"
#include "Common.h"

Car::Car(string &id) : _id(id),  _isFaulty(false), _faultyTimeLeft(0), _roadPlan(),
_distanceFromStart(0) ,  _history(){
	logger("CAR CTOR with ID: " + id);
} 

double Car::advanceCarInCurrentRoad(double speed) { 
	logger("Car::advanceCarInCurrentRoad: " + _id);

	// If the car is not faulty, advance it
	if (!(_isFaulty)) {
		_distanceFromStart = _distanceFromStart + speed;

		if (_distanceFromStart > _roadPlan.front()->getLength()) {
			logger("Car::advanceCarInCurrentRoad: " + _id + " reached end of road");
			_distanceFromStart = _roadPlan.front()->getLength();
		}
	} 
	else if (_faultyTimeLeft == 0) { // Falty time is up, mark as fixed
		logger("Car::advanceCarInCurrentRoad: " + _id + " faulty over, will advance next time.");
		_isFaulty = false;
	}
	else // Just faulty and time is not up, so decrease the faulty time remaining
	{
		logger("Car::advanceCarInCurrentRoad: " + _id + " is faulty. not advancing.");
		_faultyTimeLeft--;
	}

	return _distanceFromStart;
}

void Car::advanceInPlan(){  
	logger("Car::advanceInPlan: " + _id);

	// pop current road 
	_roadPlan.pop();

	// If we still got a road to pass, add this car to the road
	if (!(_roadPlan.empty())) {
		Road *r = _roadPlan.front();

		_distanceFromStart = 0;

		r->addCarToRoad(this);
	}
	else {
		logger("Car::advanceInPlan: " + _id + " finished its road plan.");
	}
} 

double	Car::getDistanceFromStart(){
	return _distanceFromStart;
}

vector<string>	Car::getHistory(){
	return _history;
}

string	Car::getId(){
	return _id;
}

void Car::markAsFaulty(int faultyDuration){
	logger("Car::markAsFaulty: " + _id + " for duration: " + to_string(faultyDuration));
	_isFaulty = true;
	_faultyTimeLeft = _faultyTimeLeft + faultyDuration - 1; // the -1 cause _faultyTimeLeft ends at zero
}

int Car::getFaultyTimeLeft() {
	if (!isFaulty()) return 0;

	return _faultyTimeLeft + 1;
}

bool Car::isFaulty(){
	return _isFaulty;
}

void Car::enqueuePlanItem(Road *road){
	logger("Car::enqueuePlanItem: for car: " + _id + " road: " + road->getID());

	_roadPlan.push(road);

	// If it's the first road in plan, add this car to the road
	if (_roadPlan.size() == 1) {
		road->addCarToRoad(this);
	}
}

void Car::addHistoryEvent(){ 
	// If the car finished, don't report history
	if (_roadPlan.empty()) {
		return;
	}

	Road *r = _roadPlan.front();

	// convert double to string
	ostringstream strs;
	strs << _distanceFromStart;
	string str = strs.str();
	
	// Build the history format needed
	string res = "(" + to_string(TrafficSimulation::getCurrentTime()) + "," + r->getStartJunction()->getId() + "," + r->getEndJunction()->getId() + "," + str + ")";

	_history.insert(_history.end(), res);

	logger("Car::addHistoryEvent: " + res + " to car: " + _id);
}



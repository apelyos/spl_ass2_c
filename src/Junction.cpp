#include "Road.h"
#include "Junction.h"
#include "Car.h"
#include "TrafficSimulation.h"
#include "Common.h"

Junction::Junction(string &id) : _id(id) , _incomingRoads (), _greenLightCurrentRoad(0),
_greenLightTotalTime(0), _greenLightTotalCars(0), _greenLightDuration(0) {
	logger("Junction CTOR with ID: " + id);

}

string Junction::getId() {
	return _id;
}

void Junction::advanceCars() { 
	logger("Junction::advanceCars, Junction: " + _id);

	if (_incomingRoads.size() == 0) return; // If I dont have any incoming roads, exit

	logger("Junction::advanceCars: Green light to road: " + _incomingRoads[_greenLightCurrentRoad]->getID());

	// Dequeue outgoing car from the current road that has green light
	Car *car = _incomingRoads[_greenLightCurrentRoad]->dequeueOutgoingCar();

	// If the pointer is not null, advance the given car and increase the totalcars counter
	if (car != NULL) {
		car->advanceInPlan();
		_greenLightTotalCars++;
	}

	// still got time to green light (time is always + 1)
	if ((_greenLightTotalTime + 1) < _greenLightDuration[_greenLightCurrentRoad]) {
		_greenLightTotalTime++;
	}
	else // green light is over
	{
		// update time slices:
		if (_greenLightTotalCars == (_greenLightTotalTime + 1)) { // time was used to the fullest (time is always + 1)
			_greenLightDuration[_greenLightCurrentRoad] = min(_greenLightDuration[_greenLightCurrentRoad] + 1, TrafficSimulation::MAX_TIME_SLICE);
		}
		else if (_greenLightTotalCars == 0) { // no cars passed at all
			_greenLightDuration[_greenLightCurrentRoad] = max(_greenLightDuration[_greenLightCurrentRoad] - 1, TrafficSimulation::MIN_TIME_SLICE);
		}

		// rotate green light
		_greenLightCurrentRoad = (_greenLightCurrentRoad + 1) % _incomingRoads.size();
		_greenLightTotalCars = 0;
		_greenLightTotalTime = 0;
	}
}
int Junction::getGreenLightTotalTime(){
	return _greenLightTotalTime;
}

string Junction::getCurrentRoadId(){
	return _incomingRoads[_greenLightCurrentRoad]->getID();
}

vector <Road*> Junction::getIncomingRoads(){
	return _incomingRoads;
}

void Junction::addIncomingRoad(Road *road) {
	logger("Junction::addIncomingRoad, Junction: " + _id + " Road: " + road->getID());

	// add incoming road
	_incomingRoads.insert(_incomingRoads.end(), road);

	// add the default time slice to the road with the same index
	_greenLightDuration.insert(_greenLightDuration.end(), TrafficSimulation::DEFAULT_TIME_SLICE);
}

vector<string> Junction::getTimeSlices() {
	vector<string> res;

	for (int i = 0; i < int(_incomingRoads.size()); i++) {
		if (i == _greenLightCurrentRoad) {
			res.push_back("(" + to_string(_greenLightDuration[i]) + "," + to_string(_greenLightDuration[i] - _greenLightTotalTime) + ")");
		}
		else {
			res.push_back("(" + to_string(_greenLightDuration[i]) + ",-1)");
		}
	}

	return res;
}

#include "JunctionReport.h"
#include "Common.h"
#include "Car.h"
#include "Road.h"
#include "TrafficSimulation.h"

JunctionReport::JunctionReport(int timeOfExecution, string &id, Junction* junction, boost::property_tree::ptree& reportsPt, TrafficSimulation& sim) :
Report(id, timeOfExecution, reportsPt, sim) , _junction(junction) {

}

void JunctionReport::writeReport(){
	_reportsPt.add(_id + ".junctionId", _junction->getId());

	// Get time slices from junction
	vector<string> timeSlices = _junction->getTimeSlices();
	string slicesToWrite = "";

	// Concatenate the time slices string
	for (unsigned i = 0; i < timeSlices.size(); i++){
		slicesToWrite += timeSlices[i];
	}

	_reportsPt.add(_id + ".timeSlices", slicesToWrite);

	// Get all incoming roads in the junction
	vector<Road*> roads = _junction->getIncomingRoads();

	// Iterate incoming roads
	for (unsigned i = 0; i < roads.size(); i++){
		// Get cars standing in queue
		vector<Car*> carsInQueue = roads.at(i)->getCarsInOutgoingQueue();
		string carsToWrite = "";
		
		// Concatenate the cars string
		for (int j = int(carsInQueue.size() - 1); j >= 0; j--){
			carsToWrite += "(" + carsInQueue.at(j)->getId() + ")";
		}
		_reportsPt.add(_id + "." + roads.at(i)->getStartJunction()->getId(), carsToWrite);
	}
}

JunctionReport::~JunctionReport()
{
}

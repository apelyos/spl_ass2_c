#include "CarReport.h"
#include "Car.h"
#include "Common.h"
#include "TrafficSimulation.h"

CarReport::CarReport(int timeOfExecution, string &id, string &carId, boost::property_tree::ptree& reportsPt, TrafficSimulation& sim) :
Report(id, timeOfExecution, reportsPt, sim) , _carId(carId) {

}

void CarReport::writeReport() {
	Car* car = _sim.getCar(_carId);

	if (car == NULL){
		_reportsPt.add(_id + ".Error", "Could not find: " + _carId);
		return;
	}

	_reportsPt.add(_id + ".carId", car->getId());
	string allHistory;
	for (unsigned i = 0; i < car->getHistory().size(); ++i){
		allHistory = allHistory + car->getHistory().at(i);
	}
	_reportsPt.add(_id + ".history", allHistory);
	_reportsPt.add(_id + ".faultyTimeLeft", car->getFaultyTimeLeft());
}

CarReport::~CarReport()
{
}

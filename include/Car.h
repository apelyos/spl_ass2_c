#ifndef CAR_H_
#define CAR_H_

#include "Common.h"

class Road;

class Car {
private:
	string _id;
	bool _isFaulty;
	int _faultyTimeLeft;
	queue<Road*> _roadPlan;
	double _distanceFromStart;
	vector<string> _history;

public:
	Car(string &id); // CTOR
	Car(const Car&);
	Car operator=(const Car&);
	double advanceCarInCurrentRoad(double Speed);  
	void	advanceInPlan(); 
	double	getDistanceFromStart();
	vector<string>	getHistory();
	string	getId();
	void markAsFaulty(int faultyDuration);
	bool 	isFaulty();
	void enqueuePlanItem(Road *road);
	int getFaultyTimeLeft();
	void addHistoryEvent();
};

#endif /*  JUNCTION_H_ */ 

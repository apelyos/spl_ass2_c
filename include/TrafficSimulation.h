#ifndef TRAFFICSIMULATION_H_
#define TRAFFICSIMULATION_H_

#include "Common.h"
#include "Junction.h"
#include "Car.h"
#include "Road.h"
#include "Event.h"
#include "AddCarEvent.h"
#include "CarFaultEvent.h"
#include "Report.h"
#include "RoadReport.h"
#include "CarReport.h"
#include "JunctionReport.h"


class TrafficSimulation
{
private:
	string _configurationIni;
	string _roadMapIni;
	string _commandsIni;
	string _eventsIni;
	string _reportsIni;
	int _terminationTime;
	std::map <string, Car*> _cars;
	std::map <string, Junction*> _junctions;
	std::map <string, Road*> _roads;
	std::multimap <int,Report*> _reports;
	std::multimap <int,Event*> _events;
	boost::property_tree::ptree *_reportsPt;
	static int _currentTime;

	void initiateConfiguration();
	void initiateRoadMap();
	void initiateCommands();
	void initiateEvents();
	void initiate();
	void addJunction(string id);
	void addRoad(string id, string startJunction, string endJunction, int length);
	void enqueueCommand(std::map<string, string> fields);
	void addCarFaultEvent(string eventId, int time, string roadPlan);
	void addCarArrivalEvent(string eventId, int time, string roadPlan);
	void updateTime();
	Road* getRoad(string id);
	Junction* getJunction(string id);
	void updateCarHistory();
	bool isEndOfSimulation();

public:
	Car* getCar(string id);
	void addCar(string carId, string roadPlan);
	static int MAX_SPEED;
	static int DEFAULT_TIME_SLICE;
	static int MAX_TIME_SLICE;
	static int MIN_TIME_SLICE;
	static int getCurrentTime();
	void runSimulation();

	TrafficSimulation(string &configurationIni, string &roadMapIni, string &commandsIni, string &eventsIni, string &reportsIni);
	TrafficSimulation(const TrafficSimulation&);
	TrafficSimulation operator=(const TrafficSimulation&);
	~TrafficSimulation();
};

#endif /* TRAFFICSIMULATION_H_ */



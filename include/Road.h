#ifndef ROAD_H_
#define ROAD_H_

#include "Common.h"

class Car;
class Junction;

class Road {
private:
	string _id; 
	Junction *_startJunction; 
	Junction *_endJunction;
	vector<Car*> _cars;
	int _length;
	double getDrivingSpeed(int numOfFaultyCars);

public:
	Road(Junction *startJunction, Junction *endJunction, int length); // CTOR
	Road(const Road&);
	Road operator=(const Road&);
	void advanceCars(); 
	Junction* getStartJunction();
	Junction* getEndJunction();
	void	addCarToRoad(Car *car);
	string getID();
	Car* dequeueOutgoingCar();
	int getLength();
	vector<Car*> getCars();
	vector<Car*> getCarsInOutgoingQueue();
};

bool compareByDistanceFromStart(Car* a, Car* b);

#endif /* ROAD_H_ */ 

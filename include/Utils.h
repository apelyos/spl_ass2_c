/*
 * Utils.h
 *
 *  Created on: Nov 30, 2014
 *      Author: yos
 */

#ifndef UTILS_H_
#define UTILS_H_

#include <string>

using namespace std;

void logger(string msg);

string to_string(int num);

int stoi (string str);

vector<string> split(string sentence, char delimiter);

#endif /* UTILS_H_ */

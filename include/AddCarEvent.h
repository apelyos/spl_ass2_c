#ifndef ADDCAREVENT_H_
#define ADDCAREVENT_H_

#include "Event.h"
#include "Common.h"

class TrafficSimulation;
class Event;

class AddCarEvent : public Event {
private:
	string _carId;
	string _roadPlan;
	TrafficSimulation *_sim;

public:
	AddCarEvent(int timeOfExecution, string &carID, string &roadPlan, TrafficSimulation *sim);; // TO implement
	AddCarEvent(const AddCarEvent&);
	AddCarEvent operator=(const AddCarEvent&);
	virtual ~AddCarEvent();
	void performEvent();
};

#endif /* ADDCAREVENT_H_ */ 

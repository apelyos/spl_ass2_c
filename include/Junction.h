#ifndef JUNCTION_H_
#define JUNCTION_H_

#include "Common.h"

class Road;

class Junction {
private:
	string _id;
	vector <Road*> _incomingRoads;
	int _greenLightCurrentRoad;
	int _greenLightTotalTime;
	int _greenLightTotalCars;
	vector <int> _greenLightDuration;  // Vector of time slices for each road

public:
	Junction(string &id); // CTOR
	Junction(const Junction&);
	Junction operator=(const Junction&);
	void advanceCars(); 
	string getId();
	void addIncomingRoad(Road *road);
	int getGreenLightTotalTime();
	string getCurrentRoadId();
	vector <Road*> getIncomingRoads();
	vector <string> getTimeSlices();
};

#endif /* JUNCTION_H_ */ 

#ifndef CARREPORT_H_
#define CARREPORT_H_

#include "Report.h"
#include "Common.h"
#include "Car.h"

class TrafficSimulation;
class Report;
class Car;

class CarReport : public Report
{
private:
	string _carId;
public:
	CarReport(int time, string &id, string &car, boost::property_tree::ptree& reportsPt, TrafficSimulation& sim);
	void writeReport();
	virtual ~CarReport();
};

#endif /* CARREPORT_H_ */


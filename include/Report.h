#ifndef REPORT_H_
#define REPORT_H_

#include "Common.h"

class TrafficSimulation;

class Report
{
protected:
	string _id;
	int _timeOfExecution;
	boost::property_tree::ptree& _reportsPt;
	TrafficSimulation& _sim;
public:
	Report(string id, int timeOfExecution, boost::property_tree::ptree& reportsPt, TrafficSimulation& sim);
	virtual ~Report();
	virtual void writeReport() = 0; // pure virtual method
	int getTimeOfExecution();
};
#endif /* REPORT_H_ */


#ifndef JUNCTIONREPORT_H_
#define JUNCTIONREPORT_H_

#include "Report.h"
#include "Common.h"
#include "Junction.h"

class JunctionReport : public Report
{
private:
	Junction* _junction;
public:
	void writeReport();
	JunctionReport(int time, string &id, Junction* junction, boost::property_tree::ptree& reportsPt, TrafficSimulation& sim);
	JunctionReport(const JunctionReport&);
	JunctionReport operator=(const JunctionReport&);
	virtual ~JunctionReport();
};
#endif /* JUNCTIONREPORT_H_ */


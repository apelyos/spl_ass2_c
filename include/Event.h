#ifndef EVENT_H_
#define EVENT_H_

// Abstract class event
class Event {
private:
	int _timeOfExecution;

public:
	Event(int timeOfExecution);
	virtual ~Event();
	
	virtual void performEvent() = 0; // Abstract method "pure virtual"

	int getTimeOfExecution();
};

#endif /* EVENT_H_ */ 

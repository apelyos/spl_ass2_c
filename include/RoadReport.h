#ifndef ROADREPORT_H_
#define ROADREPORT_H_

#include "Report.h"
#include "Road.h"
#include "Common.h"

class RoadReport : public Report
{
private:
	Road* _road;
public:
	void writeReport();
	RoadReport(int time, string &id, Road* road, boost::property_tree::ptree& reportsPt, TrafficSimulation& sim);
	RoadReport(const RoadReport&);
	RoadReport operator=(const RoadReport&);
	virtual ~RoadReport();
};
#endif /* ROADREPORT_H_ */


#ifndef CARFAULTEVENT_H_
#define CARFAULTEVENT_H_

#include "Event.h"
#include "Common.h"

class TrafficSimulation;
class Event;

class CarFaultEvent : public Event {
private:
	string _carId;
	int _faultDuration;
	TrafficSimulation *_sim;

public:
	CarFaultEvent(int timeOfExecution, string &carID, int faultDuration, TrafficSimulation *sim);
	CarFaultEvent(const CarFaultEvent&);
	CarFaultEvent operator=(const CarFaultEvent&);
	virtual ~CarFaultEvent();
	void performEvent();
};

#endif /* CARFAULTEVENT_H_ */ 

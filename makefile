#Vars
IFLAGS = -isystem/usr/local/boost/1.57.0/include/boost
 
# All Targets
all: RoadSimulator

# Tool invocations
# Executable "RoadSimulator" depends on the files RoadSimulator.o and run.o.
RoadSimulator: bin/Car.o bin/Junction.o bin/Road.o bin/TrafficSimulation.o bin/Event.o bin/Report.o bin/AddCarEvent.o bin/CarFaultEvent.o bin/CarReport.o bin/RoadReport.o bin/JunctionReport.o bin/Utils.o bin/Run.o 
	@echo 'Building target: RoadSimulator'
	@echo 'Invoking: C++ Linker'
	g++ -o bin/RoadSimulator bin/Car.o bin/Junction.o bin/Road.o bin/TrafficSimulation.o bin/Event.o bin/Report.o bin/AddCarEvent.o bin/CarFaultEvent.o bin/CarReport.o bin/RoadReport.o bin/JunctionReport.o bin/Utils.o bin/Run.o
	@echo 'Finished building target: RoadSimulator'
	@echo ' '

# Depends on the source and header files
bin/Car.o: src/Car.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/Car.o src/Car.cpp

# Depends on the source and header files 
bin/Junction.o: src/Junction.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/Junction.o src/Junction.cpp

# Depends on the source and header files 
bin/Road.o: src/Road.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/Road.o src/Road.cpp

# Depends on the source and header files 
bin/TrafficSimulation.o: src/TrafficSimulation.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/TrafficSimulation.o src/TrafficSimulation.cpp

# Depends on the source and header files 
bin/Event.o: src/Event.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/Event.o src/Event.cpp

# Depends on the source and header files 
bin/AddCarEvent.o: src/AddCarEvent.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/AddCarEvent.o src/AddCarEvent.cpp

# Depends on the source and header files 
bin/CarFaultEvent.o: src/CarFaultEvent.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/CarFaultEvent.o src/CarFaultEvent.cpp

# Depends on the source and header files 
bin/Report.o: src/Report.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/Report.o src/Report.cpp

# Depends on the source and header files 
bin/CarReport.o: src/CarReport.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/CarReport.o src/CarReport.cpp

# Depends on the source and header files 
bin/JunctionReport.o: src/JunctionReport.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/JunctionReport.o src/JunctionReport.cpp

# Depends on the source and header files 
bin/RoadReport.o: src/RoadReport.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/RoadReport.o src/RoadReport.cpp

# Depends on the source and header files 
bin/Utils.o: src/Utils.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/Utils.o src/Utils.cpp

# Depends on the source and header files 
bin/Run.o: src/Run.cpp
	g++ -g -Wall -Weffc++ -c -Linclude -Iinclude $(IFLAGS) -o bin/Run.o src/Run.cpp

#Clean the build directory
clean: 
	rm -f bin/*
